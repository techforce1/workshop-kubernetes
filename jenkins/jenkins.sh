#!/usr/bin/env bash

## make scripts verbose
set -ex

# Running scrips as root is bad!!
if [ "$(id -u)" == "0" ]; then
   echo "This script must not be run as root" 1>&2
   exit 1
fi

#make the namespace
kubectl create namespace jenkins
#this is bad mkay... we should not make jenkis cluster-admin....
kubectl create clusterrolebinding jenkins \
    --clusterrole cluster-admin \
    --serviceaccount=jenkins:default

#make the persistant claim (persistant data)
kubectl apply -f ./jenkins-pv.yml

#sleep to make sure the datastore is available
sleep 3

#install jenkins
helm install stable/jenkins \
      --set Persistence.ExistingClaim=jenkins-claim \
      -f ./jenkins/values.yaml \
      --namespace jenkins
