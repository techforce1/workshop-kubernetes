#!/bin/sh

#variables
REPO=kube-registry.kube-system.svc.cluster.local:31000
IMAGE=techforce1-nginx
VERSION=v1.0.1

echo "Building image: ${IMAGE} version: ${VERSION}"
echo "to private repo at: ${REPO}"
echo

#change working dir to docker base dir
cd ..
#build the image
OUTPUT=$(sudo docker build .)
if [ "$?" != "0" ];then
  echo "build failed"
  echo "output: "
  echo $OUTPUT
  exit 1
fi

#get the image hash (awk ftw!)
HASH=$(echo $OUTPUT | awk '{print $NF}')

#tag the image
sudo docker tag ${HASH} ${REPO}/${IMAGE}:${VERSION}
#push the image to the private repo
sudo docker push ${REPO}/${IMAGE}:${VERSION}
