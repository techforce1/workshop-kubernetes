#!/usr/bin/env bash

## make scripts verbose
set -ex

# Running scrips as root is bad!!
if [ "$(id -u)" == "0" ]; then
   echo "This script must not be run as root" 1>&2
   exit 1
fi

kubectl apply -f files/dns.yml

#make rbac role-bindings for certificates
kubectl apply -f files/admin.yml
kubectl apply -f files/admin-registry.yml

#change workingdir to the registry tooling submodule
SUBMODULE_PATH=$(realpath registry-tooling)
cd ${SUBMODULE_PATH}

./reg-tool.sh install-k8s-reg -y
##install the certs on the host for docker to use
sudo ./reg-tool.sh install-cert
