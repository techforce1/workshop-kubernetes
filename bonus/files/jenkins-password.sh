#!/usr/bin/env bash

echo admin
echo $(kubectl get secret --namespace jenkins $(kubectl get secret -n jenkins |grep 'jenkins '|awk '{print $1}') -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode);
